# Logan_celebrating_diversity

A dashboard which displays census stats around migrant diversity and social contributions in Australia and Logan Queensland. The dashboard allows users to compare migrant groups in Logan to the same groups nationally and explore change over time.
